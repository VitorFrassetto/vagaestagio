<?php
session_start();
include('conexao.php');

$sql = "SELECT COUNT(*) as total FROM venda WHERE entregue = 'Nao'";
$result = mysqli_query($conexao, $sql);
$row = mysqli_fetch_assoc($result);

if($row['total'] == 1) {
    $_SESSION['codigo_existe'] = true;
    
    $sql = "UPDATE venda SET entregue = 'Sim'";
    if($conexao->query($sql) === TRUE) {
        $_SESSION['status_atualizar'] = true;
        header('Location: listarvendas.php');
    }
    
	exit;
}


$conexao->close();

header('Location: listarvendas.php');
exit;
?>