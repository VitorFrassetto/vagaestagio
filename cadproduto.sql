-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 17-Set-2019 às 04:53
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cadproduto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE IF NOT EXISTS `produto` (
  `codigo` varchar(15) NOT NULL,
  `descricao` varchar(200) NOT NULL,
  `custo` float NOT NULL,
  `data_cadastro` datetime NOT NULL,
  `data_edicao` datetime NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`codigo`, `descricao`, `custo`, `data_cadastro`, `data_edicao`) VALUES
('1', 'mesa', 20, '2019-09-16 21:52:57', '2019-09-16 21:57:26'),
('1112', '1112', 1112, '2019-09-16 22:20:12', '0000-00-00 00:00:00'),
('1113', '1113', 1113, '2019-09-16 22:20:23', '0000-00-00 00:00:00'),
('1114', '1114', 1114, '2019-09-16 22:20:30', '0000-00-00 00:00:00'),
('1115', '1115', 1115, '2019-09-16 22:20:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `venda`
--

CREATE TABLE IF NOT EXISTS `venda` (
  `id` int(11) NOT NULL,
  `data_emissao` date NOT NULL,
  `data_entrega` date NOT NULL,
  `codigo` varchar(15) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `entregue` varchar(3) DEFAULT 'Nao',
  PRIMARY KEY (`id`),
  KEY `codigo_fk` (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `venda`
--

INSERT INTO `venda` (`id`, `data_emissao`, `data_entrega`, `codigo`, `quantidade`, `entregue`) VALUES
(1, '2019-09-16', '2019-09-19', '1', 20, 'Nao');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
